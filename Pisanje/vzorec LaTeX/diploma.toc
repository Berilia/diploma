\select@language {english}
\select@language {slovene}
\contentsline {chapter}{Seznam uporabljenih izrazov}{}{chapter*.1}
\contentsline {chapter}{Povzetek}{}{chapter*.2}
\select@language {english}
\contentsline {chapter}{Abstract}{}{chapter*.3}
\select@language {slovene}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Ljudska pesem}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Ozadje in zgodovina}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Zbiranje}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Klasifikacija in struktura ljudske pesmi}{6}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Razdelitev}{6}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Struktura}{6}{subsection.2.3.2}
\contentsline {chapter}{\numberline {3}Opredelitev aplikacije}{9}{chapter.3}
\contentsline {chapter}{\numberline {4}Uporabljene metode in tehnologije}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Programska oprema}{11}{section.4.1}
\contentsline {section}{\numberline {4.2}Jeziki}{11}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}HTML in CSS}{12}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}PHP}{12}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Python}{12}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Javascript}{12}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}D3.js}{13}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}jQuery}{13}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Gmaps.js}{13}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}AngularJS}{13}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Ostalo}{14}{subsection.4.3.5}
\contentsline {section}{\numberline {4.4}MDS}{14}{section.4.4}
\contentsline {chapter}{\numberline {5}Delo s podatki}{15}{chapter.5}
\contentsline {section}{\numberline {5.1}Glavna zbirka podatkov}{15}{section.5.1}
\contentsline {section}{\numberline {5.2}Matrika podobnosti}{16}{section.5.2}
\contentsline {section}{\numberline {5.3}Za\IeC {\v c}etna obdelava podatkov s Pythonom}{17}{section.5.3}
\contentsline {section}{\numberline {5.4}Obdelava v okviru aplikacije}{18}{section.5.4}
\contentsline {chapter}{\numberline {6}Postopek na\IeC {\v c}rtovanja}{19}{chapter.6}
\contentsline {section}{\numberline {6.1}Razdelitev na nivoje}{19}{section.6.1}
\contentsline {section}{\numberline {6.2}Prvi nivo - Variantni tipi}{20}{section.6.2}
\contentsline {section}{\numberline {6.3}Drugi nivo - Variante}{22}{section.6.3}
\contentsline {section}{\numberline {6.4}Tretji nivo - Besedilo}{23}{section.6.4}
\contentsline {section}{\numberline {6.5}Oblikovanje}{23}{section.6.5}
\contentsline {chapter}{\numberline {7}Implementacija}{25}{chapter.7}
\contentsline {section}{\numberline {7.1}Prvi nivo}{25}{section.7.1}
\contentsline {section}{\numberline {7.2}Drugi nivo}{28}{section.7.2}
\contentsline {section}{\numberline {7.3}Tretji nivo}{29}{section.7.3}
\contentsline {section}{\numberline {7.4}Oblikovanje}{29}{section.7.4}
\contentsline {chapter}{\numberline {8}Sklepne ugotovitve}{31}{chapter.8}
\contentsline {section}{\numberline {8.1}Mo\IeC {\v z}ne izbolj\IeC {\v s}ave}{31}{section.8.1}
\contentsline {section}{\numberline {8.2}Zaklju\IeC {\v c}ek}{32}{section.8.2}
