\select@language {english}
\select@language {slovene}
\contentsline {chapter}{Povzetek}{}{chapter*.2}
\select@language {english}
\contentsline {chapter}{Abstract}{}{chapter*.3}
\select@language {slovene}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Sklicevanje na besedilne konstrukte}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Plovke: slike in tabele}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Formati slik}{3}{subsection.1.2.1}
\contentsline {section}{\numberline {1.3}PDF/A}{4}{section.1.3}
\contentsline {chapter}{\numberline {2}O ljudskih pesmih}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Ozadje in zgodovina}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Zbiranje}{7}{section.2.2}
\contentsline {chapter}{\numberline {3}Vizualizacija podatkov}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Pomen}{9}{section.3.1}
\contentsline {chapter}{\numberline {4}Uporabljene metode in tehnologije}{11}{chapter.4}
\contentsline {chapter}{\numberline {5}Struktura, pomen in obdelava podatkov}{13}{chapter.5}
\contentsline {chapter}{\numberline {6}Postopek na\IeC {\v c}rtovanja}{15}{chapter.6}
\contentsline {chapter}{\numberline {7}Implementacija}{17}{chapter.7}
\contentsline {chapter}{\numberline {8}Sklepne ugotovitve}{19}{chapter.8}
\contentsline {chapter}{\numberline {9}Viri}{21}{chapter.9}
