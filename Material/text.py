import xml.etree.ElementTree as ET
import re
import codecs
tree = ET.parse('podatki.xml')
root = tree.getroot()
	
kraji = []
maxvar = 0;
for book in root:
	for variant in book:
		leta = []
		letas = []
		if variant.tag == "variant":
			for doc in variant:
				if doc.tag == "doc":
					for i in doc:
						if i.tag == "year":
							leto = i.text
							if leto not in leta:
								leta.append(leto) 
								letas.append(1)
							else:
								ind = leta.index(leto)
								letas[ind]+=1
						elif i.tag == "region":
							kraj = i.text
							if kraj not in kraji:
								kraji.append(kraj)
		if (len(letas) > 0):
			if (max(letas) > maxvar):
				maxvar = max(letas)					
#~ for i in kraji:
#~ print i

print maxvar

print "Min: " + min(leta) + "\n"
print "Max: " + max(leta) + "\n"
