<?php
	 
	$tip = "";
	$variant = "";
		if (isset($_GET['idt'])) {
			$tip = $_GET['idt'];
		}
		if (isset($_GET['idv'])) {
			$variant = $_GET['idv'];
		}
		
	//~ $(this).children('title').each(function() {
				//~ $('#besedilo').append($(this).text());
			//~ });
			//~ $(this).find('origtext').each(function() {
				//~ var tekst = $(this).text().split(" ");
				//~ var niz = tekst.join("</span> <span class=\"beseda\">");
				//~ $('#besedilo').append("<span class=\"beseda\">");
				//~ $('#besedilo').append(niz);
				//~ $('#besedilo').append("</span>");
				//~ return false;
			//~ });
		
?>

<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vizualizacija besedil ljudskih pesmi</title>	
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
	<script src="js/jquery.popupWindow.js"></script>
	<style>
		#body {
			background-image: url("img/bg2.png");
			overflow: scroll;
		}
		
		#besedilo {
			margin:5px;
		}

		#besedilo p {
			padding: 10px;
			margin: 10px;
		}	
		
	</style>
  </head>
  <body>
		<span id="tip" style="visibility: hidden"><?php echo $tip; ?></span><span id="variant"  style="visibility: hidden"><?php echo $variant; ?></span>
		
		<div id="besedilo" style="white-space:pre">
		</div>
		<script>
		$(document).ready(function(){	
				var variant = $("#variant").text();
				var tip = $("#tip").text();
				var filename = "vars/" + tip + ".xml";
				$.ajax({
					type:"GET",
					url: filename,
					dataType: "xml",
					success: function(xml){
						myXML = $(xml).find('variant').filter(function() {
							return $(this);
						});
						myXML.children('doc').each(function() {
							var title = $(this).children('title').text().replace(/\s+/g, '');
							if (title == variant+".") {
								$("#besedilo").append("<p style=\"weight:600\"> "+$(this).children('title').text() + " "
								+$(this).children('place').text() + ", "
								+$(this).children('region').text() + " ("
								+$(this).children('year').text() + ")</p>");
								$("#besedilo").append("<p>"+$(this).children('origtext').text()+"</p>");
								//~ ("#besedilo").append($(this).children('meta').text();
							}
						});
					},
					async: false
				});
		});
		</script>

  </body>
</html>



