<script>
	var app = angular.module("MyApp", []);

	app.controller('TodoCtrl', function($scope, $http) {
	  $http.get('seznamtipov.json').then(function(res){
		  $scope.types = res.data; 
		  
			$scope.filterFunction = function(element) {
			return element.name.match(/^Ma/) ? true : false;
			};               
		  
			//~ $scope.changeGraph = function(tip) {				
				//~ curTip = tip.st;
				//~ console.log(curTip);
				//~ getSimMatrix(1);	
			//~ };		  
		});	
	});		 
</script>

<div ng-controller="TodoCtrl">
	<div id="filter-form">
		<form class="form-inline">
			<input ng-model="query" type="text"	placeholder="Filtriraj seznam ..." autofocus>
		</form>
	</div>
	<div id="seznam-div"> 
		<ul id="seznam-ul" class="list-unstyled">
			<li ng-repeat="type in types | filter:query | orderBy: 'st' "><button class="sezlink" id="{{type.st}}" ng-click="changeGraph(type)">{{type.st}}. {{type.naslov}}</button></li>
		</ul>			
	</div>
</div>
