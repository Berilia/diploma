$(document).ready(function(){
	var tipi = [];
	var matrix = [];
	var graphx = 1100;
	var graphy = 600;
	var curTip = 60;
	var stKrog = 18;
	
	
	
	// ob refreshu oz. prvo nalaganje grafa		
	getSimMatrix(0);	
	var response;
	var oReq = new XMLHttpRequest();
	oReq.onload = reqListener;
	oReq.open("get", "tempsim.json", true);
	oReq.send();	
	
	
	$("button .sezlink").on("click", function() {
		curTip = d3.select(this).attr('id');
		console.log(curTip);
		getSimMatrix(1);
	});	

	function reqListener(e) {
		response = JSON.parse(this.responseText);
		tipi = jQuery.parseJSON(JSON.stringify(response.tipi));
		matrix = jQuery.parseJSON(JSON.stringify(response.matrix));
  
		// sestavimo matriko za mds  
		var distances = [];
		$.each(matrix, function(k,v) {
			var line = [];
			$.each(v, function(k1,v1) {
				line.push(v1['value']);
			});
			distances.push(line);
		});
		
		// json z naslovi tipov
		$.getJSON('seznamtipov2.json', function(data) {
			var seznamtipov = data;
				
			// priprava arrayev z naslovi in knjigami
			var naslovi = [];
			var bookid = [];
			var stvar = [];
			for (var i = 0; i < tipi.length; i++) {
				for (var j = 0; j < seznamtipov.length; j++) {
					if (tipi[i]['st'] == seznamtipov[j]['st']) {
						naslovi[i] = seznamtipov[j]['naslov'];
						//$('#graph').append(naslovi[i]);
						bookid[i] =  seznamtipov[j]['book'];
						stvar[i] = seznamtipov[j]['stv'];
						break;
					}
				}
			}
				
			var nodes = pripravaKoordinat(distances, naslovi, tipi, stvar);
								
			d3.selection.prototype.moveToFront = function() {
				return this.each(function(){
					this.parentNode.appendChild(this);
				});
			};			
											
			// tooltips
			var divtt = d3.select("#graph")
				.append("div") 
				.attr("class", "tooltip") 
				.style("opacity", 0); 
			
			var svg = d3.select("#graph").append("svg")
				//~ .attr("viewBox", "0 0 800 500")
				//~ .attr("preserveAspectRatio", "xMinYMin meet")
				.attr("width", graphx)
				.attr("height", graphy);
			
			svg.selectAll("circle")
				.data(nodes)
				.enter()
				.append("circle")
				.attr("cx", function(d){return d.x;})
				.attr("cy", function(d){return d.y;})
				.attr("r", function(d) {return d.r;})
				.attr("class", "krog")
				.style("fill", function(d) {return d.color;})
				.attr("id", function(d) { return d.t; })
				.attr("naslov", function(d) {return d.label;})
				.attr("stvar", function(d) {return d.stv;})
				.style("stroke", "#cba719")
				.style("opacity", 0.9);
				
			$("circle").on("mouseover", function() {		
				divtt.transition()
					.duration(300)	
					.style("opacity", 0);
				divtt.transition()
					.duration(400)	
					.style("opacity", .9);	
				divtt.html(
					'<div class="info">' + 
					d3.select(this).attr('id') + ". "  + 
					d3.select(this).attr('naslov') +  "<br/>"  + 
					'<a href="#" class="linknavartip" id="'+d3.select(this).attr('id')+
					'" alt="'+d3.select(this).attr('naslov')+
					'">Variante (' + d3.select(this).attr('stvar') + 
					")</a></div>")	 
					.style("left", parseInt(d3.select(this).attr('cx')-75) + "px")			 
					.style("top", ( parseInt(d3.select(this).attr('cy')) + (40 - parseInt(d3.select(this).attr('r'))) ) + "px");
			});
				
			$("circle").on("click", function() {
				curTip = d3.select(this).attr('id');
				console.log("clicked on first load: "+curTip);
				divtt.transition()
					.duration(100)
					.style("opacity",0);
				getSimMatrix(1);
			});	
			
			$(".sezlink").on("click", function() {
				curTip = d3.select(this).attr('id');
				console.log(curTip);
				getSimMatrix(1);
			});	
			
		});
		
	}

	$("circle").on("click", function() {
		curTip = d3.select(this).attr('id');
		console.log("clicked on update : "+curTip);
		divtt.transition()
			.duration(200)
			.style("opacity",0);
		getSimMatrix(1);
	});	
	
	$("circle").on("mouseover", function() {		
		divtt.transition()
			.duration(500)	
			.style("opacity", 0);
		divtt.transition()
			.duration(200)	
			.style("opacity", .9);	
		divtt.html(
			d3.select(this).attr('id') + ". "  + 
			d3.select(this).attr('naslov') +  "<br/>"  + 
			'<a href= "prikazvariant">Variante (' + d3.select(this).attr('stvar') + 
			")</a>")	 
			.style("left", (d3.select(this).attr('cx')) + "px")			 
			.style("top", (d3.select(this).attr('cy')) + "px");
	});	


	function getSimMatrix(update) {
		console.log("enter getSimMatrix update "+update +" : "+ curTip);
		$.ajax({
		  url:'iskanje-najblizjih.php?id='+curTip,
		  complete: function (response) {
			if (update) {
				console.log("called drawGraph from getSimMatrix");
				drawGraph(update);
			}
		  },
		  error: function () {
			  $('body').html('Bummer: there was an error!');
		  },
		  async:  false
		});
		return true;
	}
	
	function drawGraph(update) {
		console.log("enter drawGraph "+ curTip);
		$.ajax({
			url: 'tempsim.json',
			dataType: 'json',
			success: function(response) {
				tipi = jQuery.parseJSON(JSON.stringify(response.tipi));
				matrix = jQuery.parseJSON(JSON.stringify(response.matrix));
	  
				// sestavimo matriko za mds  
				var distances = [];
				$.each(matrix, function(k,v) {
					var line = [];
					$.each(v, function(k1,v1) {
						line.push(v1['value']);
					});
					distances.push(line);
				});
				
				
				// json z naslovi tipov
				$.getJSON('seznamtipov2.json', function(data) {
					var seznamtipov = data;
					
					// priprava arrayev z naslovi in knjigami
					var naslovi = [];
					var bookid = [];
					var stvar = [];
					for (var i = 0; i < tipi.length; i++) {
						for (var j = 0; j < seznamtipov.length; j++) {
							if (tipi[i]['st'] == seznamtipov[j]['st']) {
								naslovi[i] = seznamtipov[j]['naslov'];
								bookid[i] =  seznamtipov[j]['book'];
								stvar[i] = seznamtipov[j]['stv'];
								break;
							}
						}
					}
					
					var newnodes = pripravaKoordinat(distances, naslovi, tipi, stvar);
									
					d3.selection.prototype.moveToFront = function() {
						return this.each(function(){
							this.parentNode.appendChild(this);
						});
					};			
					
					var divtt = d3.select("#graph")
						.select("div .tooltip")  
						.style("opacity", 0);
					
					var svg = d3.select("svg");
					
					var circle = svg.selectAll("circle")
						.data(newnodes)
						.enter();	
				
					svg.selectAll("circle")
						.transition()
						.duration(550)
						.attr("cx", function(d){return d.x;})
						.attr("cy", function(d){return d.y;})
						.attr("r", function(d) {return d.r;})
						.style("fill", function(d) {return d.color;})
						.attr("id", function(d) { return d.t; })
						.attr("naslov", function(d) {return d.label;})
						.attr("stvar", function(d) {return d.stv;})
						.style("stroke", "#cba719")
						.style("opacity", 0.9);						
				});				
			},
			async: false
		});
	}	
	
	// https://github.com/benfred/mds.js/blob/master/mds.js
	(function(mds) {
    "use strict";
    /// given a matrix of distances between some points, returns the
    /// point coordinates that best approximate the distances using
    /// classic multidimensional scaling
    mds.classic = function(distances, dimensions) {
        dimensions = dimensions || 2;

        // square distances
        var M = numeric.mul(-0.5, numeric.pow(distances, 2));

        // double centre the rows/columns
        function mean(A) { return numeric.div(numeric.add.apply(null, A), A.length); }
        var rowMeans = mean(M),
            colMeans = mean(numeric.transpose(M)),
            totalMean = mean(rowMeans);

        for (var i = 0; i < M.length; ++i) {
            for (var j =0; j < M[0].length; ++j) {
                M[i][j] += totalMean - rowMeans[i] - colMeans[j];
            }
        }

        // take the SVD of the double centred matrix, and return the
        // points from it
        var ret = numeric.svd(M),
            eigenValues = numeric.sqrt(ret.S);
        return ret.U.map(function(row) {
            return numeric.mul(row, eigenValues).splice(0, dimensions);
        });
    };

    /// draws a scatter plot of points, useful for displaying the output
    /// from mds.classic etc
    
	}(window.mds = window.mds || {}));
    
 	function pripravaKoordinat(distances, arrayN, arrayT, arraySTV) {
		console.log("enter pripravaKoordinat");
		var coords = mds.classic(distances);
		var nodes = [];
		var minx = 3000;
		var miny = 3000;
		var maxx = 0;
		var maxy = 0;
		
		// največji in najmanjši krog
		var minstv = Math.min.apply(null, arraySTV);
		var	maxstv = Math.max.apply(null, arraySTV);
		
		// najmanjši ima radij 8, največji 40
		var minstvr = 8;
		var maxstvr = 50;
		
		$.each(coords, function(k,v) {
			// izračunamo radij
			var x = arraySTV[k] - minstv;
			var percent = x/(maxstv-minstv);
			var radij = minstvr + (maxstvr-minstvr)*percent;			
			
			
			//nodes[k] = {x: (v[0]+1), y: (v[1]+1), t: arrayT[k]['st'], label: arrayN[k]}; 
			if (k === 0) {
				nodes[k] = {x: (v[0]+1), y: (v[1]+1), t: arrayT[k]['st'], label: arrayN[k], color: "#be2422", r: radij, stv: arraySTV[k]};
			} else {				
				nodes[k] = {x: (v[0]+1), y: (v[1]+1), t: arrayT[k]['st'], label: arrayN[k], color: "#e5be16", r: radij, stv: arraySTV[k]};
			}				
				
			if (nodes[k]['x'] > maxx) {
				maxx = nodes[k]['x'];
			} else if (nodes[k]['x'] < minx) {
				minx = nodes[k]['x'];
			}
			if (nodes[k]['y'] > maxy) {
				maxy = nodes[k]['y'];
			} else if (nodes[k]['y'] < miny) {
				miny = nodes[k]['y'];
			}
			//~ var graphxs = parseInt($("#graph").css("width"));
			//~ var graphys = ((graphx*8)/5);
			//~ console.log(graphx+ " "+graphy)
			nodes[k]['x'] = (((nodes[k]['x'] - minx)/(maxx - minx))*(graphx-200))+100;
			nodes[k]['y'] = (((nodes[k]['y'] - miny)/(maxy - miny))*(graphy-200))+100;		
			//testni izpis koordinat
			//$('#graph').append(nodes[k]['x'] + ", "+nodes[k]['y']+ "<br />");
			
			// jitter			
			nodes[k]['x'] = nodes[k]['x'] + (radij) * ((Math.random()*2) - 1);
			nodes[k]['y'] = nodes[k]['y'] + (radij) * ((Math.random()*2) - 1);
			
		});
		
		return nodes;
	}						
	
	function doOverlay(tip, naslov) {
		
		$("#overlay").css("visibility", "visible");
		
		$("#vartip").empty();
				
		$("#vartip").append('<div id="zapri"><button id="zbutton">[X]</button></div> ');
		
		$("#zbutton").click(function(event) {
			$("#overlay").css("visibility", "hidden");	
		});
		
		var filename = "vars/" + tip + ".xml";
		
		// dobimo podatke o varianta za ta tip
		 $.ajax({
			type:"GET",
			url: filename,
			dataType: "xml",
			success: function(xml){
				myXML = $(xml).find('variant').filter(function() {
					return $(this);
				});
				var count = 0;
				var docs = [];
				var letnice = [];
				var soPodatkiLeto = 1;
				myXML.children('doc').each(function() {
					count++;
					var docdata = [];
					docdata['title'] = $(this).children('title').text().replace(/\./g, "");
					docdata['title'] = docdata['title'].replace(/\s+/g, '');
					docdata['region'] = $(this).children('region').text();
					docdata['place'] = $(this).children('place').text();
					docdata['year'] = $(this).children('year').text();
					docdata['meta'] = $(this).children('meta').text();
					docdata['plon'] = $(this).children('plon').text();
					docdata['plat'] = $(this).children('plat').text();
					docs.push(docdata);
					if ($.inArray(docdata['year'],letnice) == -1) {
						letnice.push(docdata['year']);
					}	
				});
				
				// izpis besede "variant"
				var printvar = "variant";
				if ((count%100) == 1) {
					printvar = "varianta";
				} else if ((count%100) == 2) {
					printvar = "varianti";
				} else if ((count%100) == 3 || (count%100) == 4) {
					printvar = "variante";
				}
				
				$("#vartip").append('<h2>'+tip+". "+naslov+ " ("+count+" "+printvar+") </h2>");
				
				
				// ZEMLJEVID
								
				$("#vartip").append('<div id="map"></div>');			
				
				// podatki za zemljevid
				var zemData = [];
				for (var i = 0; i < docs.length; i++) {
					if ((docs[i]['plon'] == "") && (docs[i]['plat'] == "")) {
						docs[i]['plat'] = Math.random() * (46.87 - 45.4) + 45.4;
						docs[i]['plon'] = Math.random() * (16.58 - 13.3) + 13.3;
					}
					if ((docs[i]['plon'] != "") && (docs[i]['plat'] != "")) {
						var ids = "#"+docs[i]['title']+".dot";
						zemData.push({lng:docs[i]['plon'], 
										lat:docs[i]['plat'], 
										title:docs[i]['title'].replace(/\s+/g, ''),
										infoWindow: {content: '<p>Varianta št.'+docs[i]['title'] + '<br />' + 
													docs[i]['place']+', '+docs[i]['region']+', '+docs[i]['year']+'</p>'},
										click: (function (t) {
											return function () {
												$(".dot").css("stroke","#be2422");
												$(".dot").css("stroke-width","1px");
												$(t).css("stroke","#fff");
												$(t).css("stroke-width","2px");
											};
										})(ids)
						});
					}
					
					 				
				}					
				
				// inicializacija zemljevida
				var map = new GMaps({
					div: '#map',
					lat: 46.048226,
					lng:  14.472400,
					zoom: 7,
				});
				
				// dodamo markerje na zemljevid
				map.addMarkers(zemData);
				
				GMaps.prototype.markerByTitle = function(title) {
					for (var i = 0; i < this.markers.length; ++m) {
						if (this.markers[i].get('title') === title) {
							return this.markers[i];
						}
					}
					return new google.maps.Marker();
				}
				
				// BARCHART				
				
				// sortiramo letnice po vrsti
				letnice.sort(function(a, b){return a-b});
				
				// najzgodnejše in najpoznejše leto z varianto tega tipa
				var minYear = letnice[0];
				var maxYear = letnice[letnice.length-1];
					
				// razdelimo variante po letnicah
				var timedata = [];
				for (var i = 0; i < letnice.length; i++) {
					var enoleto = [];
					enoleto.push(letnice[i]);
					for (var j = 0; j < docs.length; j++) {
						if (docs[j]['year'] == letnice[i]) {
							enoleto.push({j1:j,title:docs[j]['title']});
						}
					}
					timedata.push(enoleto);
				}		
				
				var graphnodes = [];
				var x = 0;
				for (var i = 0; i < timedata.length; i++) {
					for (var j = 1; j < timedata[i].length; j++) {
						if (timedata[i][0] == "") {
							//
						} else {
							graphnodes[x] = {leto: timedata[i][0], y: j, id:timedata[i][j].title.replace(/\s+/g, '')};
							x++;
						}
					}
				}				
				
				// poiščemo leto ki ima največ variant
				var maxVarPerYear = 1;
				for (var i = 0; i < timedata.length; i++) {
					if ((timedata[i].length -1) > maxVarPerYear) {
						maxVarPerYear = timedata[i].length -1;
					}
				}				
														
				// graf 
				$("#vartip").append('<div id="chart"></div>');
				
				var margin = {top: 20, right: 20, bottom: 20, left: 40}
				
				var height = maxVarPerYear*20;
				var width = 800;
				
				// setup x 
				var xValue = function(d) { return d.leto;}, // data -> value
					xScale = d3.scale.linear().range([0, width]), // value -> display
					xMap = function(d) { return xScale(xValue(d));}, // data -> display
					xAxis = d3.svg.axis().scale(xScale).orient("bottom").tickFormat(d3.format("d"));

				// setup y
				var yValue = function(d) { return d.y;}, // data -> value
					yScale = d3.scale.linear().range([height, 0]), // value -> display
					yMap = function(d) { return yScale(yValue(d));}, // data -> display
					yAxis = d3.svg.axis().scale(yScale).orient("left").tickFormat(function (d) { return ''; });

				
				// setup fill color
				var cValue = "#be2422";

				// add the graph canvas to the body of the webpage
				var svg = d3.select("#chart").append("svg")
					//~ .attr("viewBox", "0 0 800 150")
					//~ .attr("preserveAspectRatio", "xMinYMin meet")
					.attr("width", width + margin.left + margin.right)
					.attr("height", height + margin.top + margin.bottom)
					.append("g")
					.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
				
				var data = graphnodes;

				xScale.domain(d3.extent(data, function(d) { return d.leto; })).nice();
				yScale.domain([0, d3.max(data, function(d) { return d.y; })]);

				// x-axis
				svg.append("g")
					.attr("class", "x axis")
					.attr("transform", "translate(0," + height + ")")
					.call(xAxis)
					.append("text")
					.attr("class", "label")
					.attr("x", width)
					.attr("y", -6)
					.style("text-anchor", "end")
					.text("Čas");
					
				// draw dots
				svg.selectAll(".dot")
				  .data(data)
				  .enter().append("circle")
				  .attr("class", "dot")
				  .attr("id",function(d) { return d.id; })
				  .attr("r", 8)
				  .attr("cx", xMap)
				  .attr("cy", yMap)
				  .style("fill", cValue)
				  .on("click", function() {
						var title = $(this).attr('id');
						console.log(title);
						google.maps.event.trigger(map.markerByTitle(title), 'click');
					});
				  
				  
				// BESEDILA

				var str = '<p>Besedila: <a class="popuplyrics" href="besedilo.php?idt='+tip
					+'&idv='+(parseInt(docs[0]['title'].substring(0,1)))+'">'+docs[0]['title']+'</a> ';

				for (var i = 1; i < docs.length; i++) {
					str += '| <a class="popuplyrics" href="besedilo.php?idt='+tip
					+'&idv='+(parseInt(docs[i]['title'].substring(0,1)))+'">'+docs[i]['title']+'</a> ';
				}

				str += '</p>';
				$("#vartip").append(str);
				 
				// prikaz besedila

				$('.popuplyrics').popupWindow({ 
					height:600, 
					width:500, 
					top:50, 
					left:50,
					scrollbars: 1
				}); 

						
						
								
			},
			async: false
		});
			//~ $(this).children('title').each(function() {
				//~ $('#besedilo').append($(this).text());
			//~ });
			//~ $(this).find('origtext').each(function() {
				//~ var tekst = $(this).text().split(" ");
				//~ var niz = tekst.join("</span> <span class=\"beseda\">");
				//~ $('#besedilo').append("<span class=\"beseda\">");
				//~ $('#besedilo').append(niz);
				//~ $('#besedilo').append("</span>");
				//~ return false;
			//~ });
		
	}
		
	$(document).on("click", ".linknavartip", function(){
		console.log($(this).attr('id'));
		doOverlay($(this).attr('id'),$(this).attr('alt'));
	});

	$(".linknavartip").trigger("click");
	
	$(document).on("click", ".popuplyrics", function() {
		$(this).popupWindow({ 
			height:600, 
			width:500, 
			top:50, 
			left:100 
		}); 
	});
	
	$(".popuplyrics").trigger("click");
	//~ $('.popuplyrics').popupWindow({ 
					//~ height:600, 
					//~ width:500, 
					//~ top:50, 
					//~ left:50 
				//~ }); 

});


