<!DOCTYPE html>
<html lang="en" ng-app="MyApp">
  <head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vizualizacija besedil ljudskih pesmi</title>
	
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  	<script src="js/numeric-1.2.6.min.js"></script>
	<script src="js/jquery.popupWindow.js"></script>
	<script src="js/scripts.js"></script>
    <script src="js/d3/d3.min.js"></script>
	<script src="js/angular.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.js"></script>
	<!-- <link href='http://fonts.googleapis.com/css?family=Overlock:900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'> -->
	<link href='http://fonts.googleapis.com/css?family=Bitter:700' rel='stylesheet' type='text/css'>

    <link href="css/custom.css" rel="stylesheet">
    <!-- <script src="js/scripts.js"></script> -->
    
    

  </head>
  <body>
	 
    <header id="header">
		<h1>SLOVENSKE LJUDSKE PESMI</h1>
    </header>
    
    
	<div class="container-fluid">
		<div id="content">
			<div id="graph">	
			</div>			
		</div>
		<div id="sidelist">
			<div id="seznam">
				<?php include('seznam.php') ?>
			</div>
		</div>
	</div>
	
	<div id="overlay">
		<div id="vartip">
			
		</div>
	</div>
  </body>
</html>
